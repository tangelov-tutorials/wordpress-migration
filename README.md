# Wordpress Migration

This code uses Ansible and Terraform and it tries to emulate a migration from On Premise to the Cloud. It supports Amazon Web Services and Google Cloud Platform. Terraform code creates thefollowing infrastructure to emulate a migration from On Premise to the Cloud:

* An object storage system to save the statics of the Wordpress instance.
* A managed database to use it to store Wordpress installation (based in MySQL 8.0).


## Prerrequisites
To use this code you should have the accomplish the following prerrequisites:

* Python (Python 3.6+ preferred)
* Ansible (2.9+ version preferred)
* Terraform (0.14+ version)
* Packer (1.5+ only)


## How to use the code
The code is divided in different folders:

* Ansible folder contains all the scripts used to configure the VMs images in one Cloud provider.

* _$PROVIDER_ (_aws_ or _gcp_) contains the code needed to create the infrastructure

  * In the root of each folder there are several folders used by Packer to create the images, calling Ansible in the process.

  * There are two folders inside, _pre-packer_ and _post-packer_. The first one create the infrastructure needed to create the infrastructure to build the basic image and the second one to run that image in the cloud.

First you have to configure the Ansible variables in _ansible/vars/vars.yml_ and later you should decide where you are going to deploy the code.

Second you have to enter in _aws_ or _gcp_ and execute the code inside _pre-packer_ folder using:

```bash
terraform init

terraform apply
```

The third step is to create the image in the selected cloud provider with Packer. 

You should go to the root folder in _gcp_ or _aws_ and create a file called _variables.pkrvars.hcl_ with the following content:

```bash
# For AWS
aws_credentials_file = # Path of the AWS credentials
image_naming = # Base image name
aws_security_groups = # Assign the following security groups in the process of the image creation
aws_iam_profile = # Name of the IAM profile assigned to the AWS instance used to create the image

# For GCP
gcp_credentials_json = # Path of the GCP credentials
gcp_project_id = # Project ID where the image will be created
image_naming = # Base image name
```

After filling the variables you can create the image:

```bash
# We create the image after creating the infraestructure and the variables for Packer and Ansible
packer build -var-file=variables.pkrvars.hcl .
```
