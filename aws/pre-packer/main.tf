# RDS Module to host the Wordpress database
module "wordpress_rds" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 4.0"

  identifier = "${var.wordpress_db_name}-rds"

  engine         = var.wordpress_instance_rds_engine
  engine_version = var.wordpress_instance_rds_engine_version
  instance_class = var.wordpress_instance_rds_tier

  multi_az = true

  allocated_storage = 5

  db_name  = var.wordpress_db_name
  username = var.wordpress_db_user
  password = var.wordpress_db_password
  port     = var.wordpress_instance_rds_port

  vpc_security_group_ids = [data.aws_security_group.default.id]

  maintenance_window = "Tue:01:00-Tue:04:00"
  backup_window      = "04:00-06:00"

  # Enhanced Monitoring
  create_monitoring_role = true
  monitoring_interval    = "30"
  monitoring_role_name   = "wordpress-monitoring-rds-role"

  tags = var.custom_labels

  # DB subnet group
  subnet_ids = data.aws_subnet_ids.all.ids

  # DB parameter group
  family = var.wordpress_instance_rds_parameter_group

  # DB option group
  major_engine_version = var.wordpress_instance_rds_engine_major_version

  # Snapshot prefix name upon DB deletion
  final_snapshot_identifier_prefix = "${var.wordpress_db_name}-rds"

  # Database Deletion Protection
  deletion_protection = true

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8"
    },
    {
      name  = "character_set_server"
      value = "utf8"
    }
  ]
}
