# Data to get information of Default VPC
data "aws_vpc" "default" {
  default = true
}

# Data to get information about all Subnets in Default VPC
data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

# Data to get information about the default Security Group in Default VPC
data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.default.id
  name   = "default"
}