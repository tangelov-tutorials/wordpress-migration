module "wp_cloud_storage" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "~> 3.0"

  bucket = var.wordpress_aws_bucket
  tags   = var.custom_labels
}

# Role to access to S3 from WP instances
resource "aws_iam_role" "wp_s3_role" {
  name = var.wordpress_s3_iam_role_name
  tags = var.custom_labels

  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

# Policy to assign allow roles to be assigned to EC2 instances
data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com",
      ]
    }
  }
}

# Policy to allow access to S3 bucket
resource "aws_iam_policy" "wp_s3_policy" {
  name        = var.wordpress_s3_iam_policy_name
  description = "Policy to connect to a S3 bucket for Worpdress"

  policy = data.aws_iam_policy_document.wp_s3_policy.json
}

# Content to create a policy to access to WP S3 bucket
data "aws_iam_policy_document" "wp_s3_policy" {
  statement {
    sid    = "ManagementBucket"
    effect = "Allow"
    actions = [
      "s3:ListBucket",
      "s3:GetBucketLocation",
      "s3:ListAllMyBuckets",
      "s3:PutBucketAcl",
    ]
    resources = [
      "arn:aws:s3:::${var.wordpress_aws_bucket}",
    ]
  }

  statement {
    sid    = "AllowObjectActions"
    effect = "Allow"
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:DeleteObject",
      "s3:PutObjectAcl",
    ]
    resources = [
      "arn:aws:s3:::${var.wordpress_aws_bucket}/*",
    ]
  }
}

# Assignment between role and policy for access to S3
resource "aws_iam_role_policy_attachment" "wp_s3_role_attachment" {
  role       = aws_iam_role.wp_s3_role.name
  policy_arn = aws_iam_policy.wp_s3_policy.arn
}

# Creating IAM instance profile to connect EC2 instances to S3
resource "aws_iam_instance_profile" "wordpress_s3_iam_profile" {
  name = var.wordpress_s3_iam_profile
  role = aws_iam_role.wp_s3_role.name
}
