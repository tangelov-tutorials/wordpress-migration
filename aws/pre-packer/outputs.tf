# Outputs for AWS Terraform code
output "instance_private_conn" {
  value       = module.wordpress_rds.db_instance_endpoint
  description = "Database endpoint to connect from Wordpress instances"
}
