# Fichero de variables
variable "aws_credentials_file" {
  type        = string
  description = "Path to AWS credentials file"
}

variable "aws_region" {
  type        = string
  default     = "eu-west-1"
  description = "Default region to deploy in Amazon Web Services"
}

variable "aws_zone" {
  type        = string
  default     = "eu-west1-1a"
  description = "Default zone to deploy in Amazon Web Services"
}

variable "image_naming" {
  type        = string
  description = "Template name for the images created"
}

variable "aws_security_groups" {
  type        = list(string)
  description = "List of Security Groups to attach to the EC2 Instance"
}

variable "aws_iam_profile" {
  type        = string
  description = "Name of the IAM Profile to be used to connect to S3"
}

variable "username" {
  type        = string
  default     = "ubuntu"
  description = "Default username used to customize the base machine"
}
