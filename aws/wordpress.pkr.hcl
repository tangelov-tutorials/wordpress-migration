# Fichero de configuración
# "timestamp" template function replacement
locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }

# Sources used in Packer 1.5+
source "amazon-ebs" "aws_ec2_tangelov" {
  shared_credentials_file = var.aws_credentials_file
  ami_name             = "${var.image_naming}-${local.timestamp}"
  instance_type        = "t2.micro"
  iam_instance_profile = var.aws_iam_profile
  source_ami_filter {
    filters = {
      virtualization-type = "hvm"
      name = "ubuntu/images/*ubuntu-focal-20.04-amd64-server-*"
      root-device-type = "ebs"
    }
    owners = ["099720109477"]
    most_recent = true
  }
  security_group_ids = var.aws_security_groups
  ssh_username        = var.username
  region              = var.aws_region
}

# Builds used in Packer 1.5+
build {
  sources = [
    "source.amazon-ebs.aws_ec2_tangelov"
  ]

  provisioner "shell" {
    inline = ["sleep 90", "sudo apt-get update && sudo apt-get install python3-pip -y"]
  }

  provisioner "file" {
    source = "../ansible/"
    destination = "/tmp/"
  }

  provisioner "shell" {
    script = "../ansible/launcher.sh"
  }
}
