# General variables
variable "aws_default_region" {
  type        = string
  default     = "eu-west-1"
  description = "Default region of the whole environment"
}

variable "custom_labels" {
  type = map(any)
  default = {
    app         = "wp-tangelov"
    environment = "pro"
    ccenter     = "marketing"
  }
  description = "Labels to be added to all resources in the project"
}

# Variables related with Wordpress RDS Database
variable "wordpress_instance_rds_tier" {
  type        = string
  default     = "db.t3.micro"
  description = "Default tier for Wordpress instances in Amazon RDS"
}

variable "wordpress_instance_rds_engine" {
  type        = string
  default     = "mysql"
  description = "Default engine for Wordpress instances in Amazon RDS"
}

variable "wordpress_instance_rds_engine_version" {
  type        = string
  default     = "8.0.20"
  description = "Default engine version for Wordpress instances in Amazon RDS"
}

variable "wordpress_instance_rds_parameter_group" {
  type        = string
  default     = "mysql8.0"
  description = "Default parameter group for Wordpress instances in Amazon RDS"
}

variable "wordpress_instance_rds_engine_major_version" {
  type        = string
  default     = "8.0"
  description = "Default major engine version for Wordpress instances in Amazon RDS"
}

variable "wordpress_instance_rds_port" {
  type        = string
  default     = "3306"
  description = "Default port to connect to Wordpress RDS Instance"
}

variable "wordpress_db_name" {
  type        = string
  default     = "wordpressdb"
  description = "Default name for Wordpress database in Amazon RDS instance"
}

variable "wordpress_db_user" {
  type        = string
  default     = "wpdbuser"
  description = "Default DB user to connect to Wordpress database in Amazon RDS instance"
}

variable "wordpress_db_password" {
  type        = string
  description = "Variable to use for change the password to connect from Wordpress to Amazon RDS instance"
}


# Variables related with Wordpress S3 buckets
variable "wordpress_aws_bucket" {
  type        = string
  default     = "wp-tangelov-bucket"
  description = "Variable to use to create the name of the Wordpress S3 bucket"
}

# Variables related with S3 IAM permissions
variable "wordpress_s3_iam_role_name" {
  type        = string
  default     = "wp-s3-role"
  description = "Variable to use to create the name of the Wordpress IAM Role"
}

variable "wordpress_s3_iam_policy_name" {
  type        = string
  default     = "wp-s3-policy"
  description = "Variable to use to create the name of the Wordpress IAM Policy"
}

variable "wordpress_s3_iam_profile" {
  type        = string
  default     = "wp-s3-access"
  description = "Variable to use to create the name of the Wordpress IAM Profile to access to S3 from EC2 instances"
}
