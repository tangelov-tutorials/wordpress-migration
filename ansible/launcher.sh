#!/bin/bash

# Installing Ansible and netaddr
pip3 install ansible==5.4.0 --user
pip3 install netaddr --user

# Reloading files from bash
source ~/.bashrc
source ~/.profile

cd /tmp

# Ansible installation
ansible-galaxy collection install -r requirements.yml --force
ansible-galaxy role install -r /tmp/requirements.yml --force
ansible-playbook install-wp.yml
