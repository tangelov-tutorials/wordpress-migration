# Fichero de variables
variable "gcp_credentials_json" {
  type        = string
  description = "Path to GCP JSON credentials"
}

variable "gcp_project_id" {
  type        = string
  description = "ID of the Project in Google Cloud"
}

variable "gcp_region" {
  type        = string
  default     = "europe-west1"
  description = "Default region to deploy in Google Cloud Platform"
}

variable "gcp_zone" {
  type        = string
  default     = "europe-west1-b"
  description = "Default zone to deploy in Google Cloud Platform"
}

variable "image_naming" {
  type        = string
  description = "Template name for the images created"
}

variable "username" {
  type        = string
  default     = "administrator"
  description = "Default username used to customize the base machine"
}