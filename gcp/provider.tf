terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.13"
    }
  }
}

provider "google" {
  project = var.gcp_default_project
  region  = var.gcp_default_region
}
