# Fichero de configuración
# "timestamp" template function replacement
locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }

# Sources used in Packer 1.5+
source "googlecompute" "gcp_compute_tangelov" {
  account_file        = var.gcp_credentials_json
  image_family        = var.image_naming
  image_name          = "${var.image_naming}-${local.timestamp}"
  machine_type        = "f1-micro"
  project_id          = var.gcp_project_id
  source_image_family = "ubuntu-2004-lts"
  scopes              = [
    "https://www.googleapis.com/auth/sqlservice.admin"
  ]
  ssh_username        = var.username
  region              = var.gcp_region
  zone                = var.gcp_zone
}

# Builds used in Packer 1.5+
build {
  sources = [
    "source.googlecompute.gcp_compute_tangelov"
  ]

  provisioner "shell" {
    inline = ["sleep 90", "sudo apt-get update && sudo apt-get install python3-pip -y"]
  }

  provisioner "file" {
    source = "../ansible/"
    destination = "/tmp/"
  }

  provisioner "shell" {
    script = "../ansible/launcher.sh"
  }
}
