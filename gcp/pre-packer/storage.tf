resource "google_service_account" "wp_service_account" {
  account_id   = "wordpress"
  display_name = "Wordpress GCP SA"
  description  = "Account to manage objects from Wordpress to GCS Bucket"
}

module "wp-cloud-storage" {
  source  = "terraform-google-modules/cloud-storage/google//modules/simple_bucket"
  version = "3.1.0"

  name = var.wordpress-gcs-bucket

  labels = var.custom_labels

  iam_members = [
    {
      role   = "roles/storage.admin"
      member = "serviceAccount:${google_service_account.wp_service_account.account_id}@${var.gcp_default_project}.iam.gserviceaccount.com"
    }
  ]

  project_id = var.gcp_default_project

  location = upper(var.gcp_default_region)

  depends_on = [google_service_account.wp_service_account]
}
