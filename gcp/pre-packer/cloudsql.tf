# Terraform code to create Cloud SQL instances and private connectin

module "cloudsql-private-conn" {
  source  = "GoogleCloudPlatform/sql-db/google//modules/private_service_access"
  version = "10.0.0"

  project_id  = var.gcp_default_project
  vpc_network = data.google_compute_network.network.name
}

module "wordpress-cloudsql" {
  source  = "GoogleCloudPlatform/sql-db/google//modules/safer_mysql"
  version = "10.0.0"

  name = var.wordpress-instance-name
  tier = var.wordpress-instance-tier

  deletion_protection = false

  db_name          = var.wordpress-db-name
  db_charset       = var.wordpress-db-charset
  db_collation     = var.wordpress-db-collation
  database_version = "MYSQL_8_0"

  additional_users = [
    {
      name     = var.wordpress-db-user
      password = var.wordpress-db-password
      host     = "%"
      type     = "BUILT_IN"
    }
  ]

  user_name = "administrator"

  user_labels = var.custom_labels

  availability_type = var.wordpress-instance-availability-type
  region            = var.gcp_default_region
  zone              = "${var.gcp_default_region}-b"

  assign_public_ip   = "false"
  vpc_network        = data.google_compute_network.network.self_link
  allocated_ip_range = module.cloudsql-private-conn.google_compute_global_address_name

  project_id = var.gcp_default_project

  module_depends_on = [module.cloudsql-private-conn.peering_completed]
}

# Terraform code to enable Cloud SQL Admin API to allow to use Cloud SQL Proxy
resource "google_project_service" "cloudsql-admin-api" {
  project = var.gcp_default_project
  service = "sqladmin.googleapis.com"

  disable_dependent_services = true
}
