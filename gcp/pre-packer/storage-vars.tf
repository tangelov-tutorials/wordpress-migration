# CloudSQL vars
variable "wordpress-instance-name" {
  type        = string
  default     = "tangelov-csql-wordpress"
  description = "Default name for Wordpress instance in Cloud SQL"
}

variable "wordpress-instance-tier" {
  type        = string
  default     = "db-n1-standard-1"
  description = "Default tier for Wordpress instances in Cloud SQL"
}

variable "wordpress-instance-availability-type" {
  type        = string
  default     = "REGIONAL"
  description = "Default availability type for Wordpress instances in Cloud SQL"
}

variable "wordpress-db-name" {
  type        = string
  default     = "wordpressdb"
  description = "Default name for Wordpress database in Cloud SQL instance"
}

variable "wordpress-db-charset" {
  type        = string
  default     = "utf8"
  description = "Default charset for Wordpress database in Cloud SQL instance"
}

variable "wordpress-db-collation" {
  type        = string
  default     = "utf8_general_ci"
  description = "Default collation for Wordpress database in Cloud SQL instance"
}

variable "wordpress-db-user" {
  type        = string
  default     = "wpdbuser"
  description = "Default DB user to connect to Wordpress database in CloudSQL instance"
}

variable "wordpress-db-password" {
  type        = string
  description = "Variable to use for change the password to connect from Wordpress to Cloud SQL instance"
}

# Cloud Storage variables

variable "wordpress-gcs-bucket" {
  type        = string
  default     = "wp-tangelov-bucket"
  description = "Variable to use to create the name of the Wordpress bucket"
}
