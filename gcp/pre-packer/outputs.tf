output "administrator_user_password" {
  value     = module.wordpress-cloudsql.generated_user_password
  sensitive = true
}

output "instance_connection_name" {
  value = module.wordpress-cloudsql.instance_connection_name
}

output "instance_private_ip" {
  value = module.wordpress-cloudsql.private_ip_address
}
