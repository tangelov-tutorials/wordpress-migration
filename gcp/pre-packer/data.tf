# Data to get the project number for IAM accounts
data "google_project" "project" {
  project_id = var.gcp_default_project
}

# Data to get the VPC where the resources are located
data "google_compute_network" "network" {
  name = "default"
}
